# Project Title

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Java 1.8.0
```

### Installing

For the installation of this package you will need to clone this project onto your machine. This can be done with the following command.

```
git clone https://NathanNinja@bitbucket.org/NathanNinja/thema-9-ml.git
```

Usage

Go to this path in this project: \out\CliJar and run it like this in the commandline for a single instance from the command line:

```
 java -jar CLIdemo-Jar-1.1-SNAPSHOT.jar -i testdata/Cleanest10k.arff -o testdata/text.txt -a 2 -g 1 -r 2 -s 3 -c 1 -f 1 -sh 1 -rs 1 -ps 1
```
Or run it like this to give it a file with instances from a csv file:

```
 java -jar CLIdemo-Jar-1.1-SNAPSHOT.jar -i testdata/Cleanest10k.arff -o testdata/text.txt -u testdata/Cleanest.csv
```

Or run it like this to give it a file with instances from a Arff file:

```
 java -jar CLIdemo-Jar-1.1-SNAPSHOT.jar -i testdata/Cleanest10k.arff -o testdata/text.txt -u testdata/Cleanest.arff
```

-u means the file with instances without the right class and -o is the outputfile, name the file as you want. 


## Running the tests

Explain how to run the automated tests for this system


### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Weka](https://www.cs.waikato.ac.nz/ml/weka/) - The theorie behind Weka
* [Clidemo](https://maven.apache.org/) - Command line interface API

## Authors

* **Nathan Weesie** - *Initial work* 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


