/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.arguments_provider_demo;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import javax.sound.midi.Sequence;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author michiel
 */
public class ApacheCliOptionsProvider implements OptionsProvider {

    private static final String HELP = "help";
    private static final String AGE = "age";
    private static final String GENDER = "gender";
    private static final String RACE = "race";
    private static final String SUB = "substance";
    private static final String SERVICE = "service";
    private static final String FREQ = "frequency";
    private static final String SELFHELP = "selfhelp";
    private static final String REASON = "reason";
    private static final String PSYPROB = "psyprob";
    private static final String INPUTFILE = "file.arff";
    private static final String UNKNOWNFILE = "unkownfile.arff";
    private static final String OUTPUTFILE = "outputfile.arff";


    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private int age;
    private int gender;
    private int race;
    private int substance;
    private int service;
    private int frequency;
    private int selfhelp;
    private int reason;
    private int psyprob;
    private int LOS;
    private String soloinstance;

    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option genderOption = new Option("g", GENDER, true, "The gender of the subject (1 = Male, 2 = Female, 9 = Unknown)");
        Option ageOption = new Option("a", AGE, true, "Patients age catagory");
        Option raceOption = new Option("r", RACE, true, "Patients race catagory");
        Option subOption = new Option("s", SUB, true, "The type of substance the patient is using, default set to 'None'");
        Option servOption = new Option("c", SERVICE, true, "The type of service the patient received prior to admission");
        Option freqOption = new Option("f", FREQ, true, "Patients drug use frequency");
        Option shOption = new Option("sh", SELFHELP, true, " Frequency of attendance at substance use self-help groups in the 30 days prior to admission");
        Option reasonOption = new Option("rs", REASON, true, "REASON OF DISCHARGE");
        Option psyOption = new Option("ps", PSYPROB, true, " Co-occurring mental and substance use disorders");

        Option inputOption = new Option("i", INPUTFILE, true, "Inputfile in .arff format");
        Option unknownfileOption = new Option("u", UNKNOWNFILE, true, "Unknownfile in .arff format");
        Option outputOption = new Option("o", OUTPUTFILE, true, "outputfile in .txt format");

        options.addOption(inputOption);
        options.addOption(unknownfileOption);
        options.addOption(outputOption);



        options.addOption(helpOption);
        options.addOption(genderOption);
        options.addOption(ageOption);
        options.addOption(raceOption);
        options.addOption(subOption);
        options.addOption(servOption);
        options.addOption(freqOption);
        options.addOption(shOption);
        options.addOption(reasonOption);
        options.addOption(psyOption);

    }

    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

                if (commandLine.hasOption(INPUTFILE) && commandLine.hasOption(OUTPUTFILE) && commandLine.hasOption(UNKNOWNFILE)) {
                    WekaRunner runner = new WekaRunner();
                    runner.start(this.commandLine.getOptionValue(INPUTFILE), this.commandLine.getOptionValue(UNKNOWNFILE), this.commandLine.getOptionValue(OUTPUTFILE));
                }
                else if (commandLine.hasOption(INPUTFILE) && commandLine.hasOption(OUTPUTFILE) && commandLine.hasOption(AGE) && commandLine.hasOption(GENDER) && commandLine.hasOption(RACE) && commandLine.hasOption(SUB)
                 && commandLine.hasOption(SERVICE) && commandLine.hasOption(FREQ) && commandLine.hasOption(SELFHELP) && commandLine.hasOption(REASON) && commandLine.hasOption(PSYPROB)){

                    DataSource source = new DataSource(this.commandLine.getOptionValue(INPUTFILE));
                    Instances data = source.getDataSet();
                    if (data.classIndex() == -1 ){
                        data.setClassIndex(data.numAttributes() - 1);
                    }
                    Instance instance = new DenseInstance(10) {
                    };
                    System.out.println(data);
                    instance.setDataset(data);
                    instance.setValue(0, this.commandLine.getOptionValue(AGE));
                    instance.setValue(1, this.commandLine.getOptionValue(GENDER));
                    instance.setValue(2, this.commandLine.getOptionValue(RACE));
                    instance.setValue(3, this.commandLine.getOptionValue(SUB));
                    instance.setValue(4, this.commandLine.getOptionValue(SERVICE));
                    instance.setValue(5, this.commandLine.getOptionValue(FREQ));
                    instance.setValue(6, this.commandLine.getOptionValue(SELFHELP));
                    instance.setValue(7, this.commandLine.getOptionValue(REASON));
                    instance.setValue(8, this.commandLine.getOptionValue(PSYPROB));


                    WekaRunner runner = new WekaRunner();

                    runner.start2(this.commandLine.getOptionValue(INPUTFILE), instance, this.commandLine.getOptionValue(OUTPUTFILE));

                }

//            if (commandLine.hasOption(AGE)) {
//                String s = commandLine.getOptionValue(AGE).trim();
//                System.out.println(s);
//            }

            //check correct age format
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * legal values are 1, 2, 3
     * @return
     * @param s
     */
    private boolean isLegalVerbosityValue(String s) {
        try{
            int i = Integer.parseInt(s);
            if (i > 0 && i <= 3) {
                return true;
            } else {
                System.out.println("To high of a verbosity level");
                return false;
            }
        } catch (NumberFormatException ex) {
            return false;
        }
    }
    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MyCoolTool", options);
    }

    @Override
    public String getUserName() {
        return this.commandLine.getOptionValue(GENDER, "1");
    }

    @Override
    public VerbosityLevel getVerbosityLevel() {
        return null;
    }

    @Override
    public int getUserAge() {
        return age;
    }

}
